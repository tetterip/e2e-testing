SELENIUM-WEBDRIVER
---
Selenium is a browser automation library. Most often used for testing web-applications, Selenium may be used for any task that requires automating interaction with the browser.

INSTALLATION
---
Selenium may be installed via npm with

`npm install selenium-webdriver`

You will need to download additional components to work with each of the major browsers. The drivers for Chrome, Firefox, and Microsoft's IE and Edge web browsers are all standalone executables that should be placed on your system PATH. Apple's safaridriver is shipped with Safari 10 for OS X El Capitan and macOS Sierra. You will need to enable Remote Automation in the Develop menu of Safari 10 before testing.

[Chromedriver(.exe)](http://chromedriver.storage.googleapis.com/index.html)
[Firefox(geckodriver(.exe))](https://github.com/mozilla/geckodriver/releases/)

Usage
---

The sample below and others are included in the example directory. You may also find the tests for selenium-webdriver informative.
```
const {Builder, By, Key, until} = require('selenium-webdriver');

let driver = new Builder()
    .forBrowser('firefox')
    .build();

driver.get('http://www.google.com/ncr');
driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
driver.wait(until.titleIs('webdriver - Google Search'), 1000);
driver.quit();
```

Documentation
---
API documentation is available online from the [Selenium project](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/).Additional resources include :

* the #selenium channel on freenode IRC
* the [selenium-users@googlegroups.com](https://groups.google.com/forum/#!forum/selenium-users) list
* [SeleniumHQ](http://www.seleniumhq.org/docs/) documentation  

Node Support Policy
---
Each version of selenium-webdriver will support the latest semver-minor version of the LTS and stable Node releases. All semver-major & semver-minor versions between the LTS and stable release will have "best effort" support. Following a Selenium release, any semver-minor Node releases will also have "best effort" support. Releases older than the latest LTS, semver-major releases, and all unstable release branches (e.g. "v.Next") are considered strictly unsupported.

License
---
Licensed to the Software Freedom Conservancy (SFC) under one or more contributor license agreements. See the NOTICE file distributed with this work for additional information regarding copyright ownership. The SFC licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.