//random registration test.
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');
var Chance = require('chance');
var chance = new Chance();

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : chance.email()
};

const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);