var test = require('selenium-webdriver/testing');
var selenium = require('selenium-webdriver');
var test1 = require('./emailVerification1.js');
var test2 = require('./emailVerification2.js');
var test3 = require('./emailVerification3.js');
var test4 = require('./nameIsSpaces.js');
var test5 = require('./passHasSpaces.js');
var test6 = require('./passIsSpaces.js');
const timeOut = 15000;
test.describe("Registration Page - other scenarios", function(){
  this.timeout(timeOut);

test.beforeEach(function(){
    var webdriver = require('selenium-webdriver'),
      By = webdriver.By,
      until = webdriver.until;   
    driver = new webdriver.Builder()
      .forBrowser('chrome')
      .build();
  driver.get('http://www.pollfish.com/signup/researcher');
});

test.before(function() {

});

test.afterEach(function(){
     driver.quit();
});

test.it('Check the Email text field that has Email address without @ symbol.',function(done){
    test1.module1();
    done();
});
test.it('Check the Email text field that has random string instead of real email.',function(done){
    test2.module2();
    done();
});
test.it('Check the Email text field that has missing dot(no tld) in the email address.',function(done){
    test3.module3();
    done();
});
test.it('Checks if user can sign up when name  is just blank spaces.',function(done){
    test4.module4();
    done();
});
test.it('Checks if user can sign up when password is has some blank spaces',function(done){
    test5.module5();
    done();
});
test.it('Checks if user can sign up when password is just blank spaces',function(done){
    test6.module6();
    done();
});
});