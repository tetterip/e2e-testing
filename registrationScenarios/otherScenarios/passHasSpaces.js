//Checks if user can sign up when password has some blank spaces.
var module5 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length : 2})+'  '+chance.hash({length : 2}),
    email : chance.email()
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module5 = module5;