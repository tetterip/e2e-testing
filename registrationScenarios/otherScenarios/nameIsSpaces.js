//Checks if user can sign up when name  is just blank spaces.
var module4 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: '    ',
    pass : chance.hash({length: 10}),
    email : chance.email()
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module4 = module4;