//Check the Email text field that has random string instead of real email.
var module2 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : chance.hash({length: 10})
};

const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module2 = module2;