//  Check the Email text field that has Email address without @ symbol.
var module1 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : 'testing.com'
};

const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module1 = module1;