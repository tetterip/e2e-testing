//Check the Email text field that has missing dot(no tld) in the email address.
var module3 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : chance.email({domain: 'example'})
};

const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module3 = module3;