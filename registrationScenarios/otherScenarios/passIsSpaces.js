//Checks if user can sign up when password is just blank spaces.
var module6 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: chance.name(),
    pass : '    ',
    email : chance.email()
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module6 = module6;