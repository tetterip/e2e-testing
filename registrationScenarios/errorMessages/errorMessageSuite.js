var test = require('selenium-webdriver/testing');
var selenium = require('selenium-webdriver');
var test1 = require('./mailErrorMessage1.js');
var test2 = require('./mailErrorMessage2.js');
var test3 = require('./nameErrorMessage.js');
var test4 = require('./passErrorMessage.js');
const timeOut = 15000;

var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;	

test.beforeEach(function(){
    this.timeout(timeOut);
    driver = new webdriver.Builder()
        .forBrowser('chrome')
        .build();
	driver.get('http://www.pollfish.com/signup/researcher');

	});
test.before(function() {

});

test.afterEach(function(){
	  driver.quit();
    });

test.describe("Registration Page - Error message testing", function(){

test.it("Check if error message pops up when mail already exists", function(){
	//Check if error message pops up when mail already exists.
	test1.error1();
});

test.it("Check if error message pops up when email field left empty", function(){
    //Check if error message pops up when email field left empty
    test2.error2();
    });
test.it("Check if error message pops up when name field left empty", function(){
	//Check if error message pops up when name field left empty
	test3.error3();
});

test.it("Check if error message pops up when pass field left empty", function(){
	//Check if error message pops up when pass field left empty
	test4.error4();
});
});