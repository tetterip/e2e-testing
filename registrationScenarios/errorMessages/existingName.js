//Check if error message pops up when name already exists.
var module4 = function(){

var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/page-objects.js');

const options = {
    name: 'a',
    pass : '1234',
    email : chance.email(),
    targetclass : 'pf-form__message pf-form__message--error',
    errormessage : 'Name already exists.Please enter another one.'
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email);
}
exports.module4 = module4;