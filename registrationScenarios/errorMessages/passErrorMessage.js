//Check if error message pops up when name field left empty.
var error4 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/errormessages.js');

const options = {
    name:chance.name(),
    pass : '',
    email : chance.email(),
    targetclass : 'pf-form__message pf-form__message--error',
    erromessage : 'Your password must be at least 4 characters long.',
    y : 2
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email,options.targetclass,options.erromessage,options.y);
}
exports.error4 = error4;