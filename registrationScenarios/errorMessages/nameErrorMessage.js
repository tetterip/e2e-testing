//Check if error message pops up when name field left empty.
var error3 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/errormessages.js');

const options = {
    name:'',
    pass : chance.hash({length: 10}),
    email : chance.email(),
    targetclass : 'pf-form__message pf-form__message--error',
    erromessage : 'Please enter your full name.',
    y : 0
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email,options.targetclass,options.erromessage,options.y);
}
exports.error3 = error3;