//Check if error message pops up when email field left empty.
var error2 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/errormessages.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : '',
    targetclass : 'pf-form__message pf-form__message--error',
    erromessage : 'Please enter a valid email address.',
    y : 1
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email,options.targetclass,options.erromessage,options.y);
}
exports.error2 = error2;