//Check if error message pops up when mail already exists
var error1 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/errormessages.js');

const options = {
    name: chance.name(),
    pass : chance.hash({length: 10}),
    email : 'testing@test',
    targetclass : 'pf-form__message pf-form__message--error',
    errormessage : 'Email already exists.Please enter another one.',
    y : 1
};
const elem = new ElementsClass(options);

elem.init(options.name,options.pass,options.email,options.targetclass,options.errormessage,options.y);
}
exports.error1 = error1;