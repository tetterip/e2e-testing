//checking if email field exists.
var moduleMail = function(){

const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/ifexists.js');

const options = {
    type: "input[type='email']"
};

const elem = new ElementsClass(options);

elem.init(options.type);
}
exports.moduleMail = moduleMail;