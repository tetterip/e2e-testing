//checking if password field exists.
var modulePass = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/ifexists.js');

const options = {
    type: "input[type='password']"
};

const elem = new ElementsClass(options);

elem.init(options.type);
}

exports.modulePass = modulePass;
