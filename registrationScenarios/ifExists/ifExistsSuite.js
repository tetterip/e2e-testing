var test = require('selenium-webdriver/testing');
var selenium = require('selenium-webdriver');
var pass = require('./passwordFieldExists.js');
var name = require('./nameFieldExists.js');
var email = require('./emailFieldExists.js');
var button = require('./signupButtonExists.js');
const timeOut = 15000;
test.describe("Registration Page - if fields exist testing", function(){
  this.timeout(timeOut);

test.beforeEach(function(){

  driver.get('http://www.pollfish.com/signup/researcher');

  });
test.before(function() {
  var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;   
  driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();
});

test.after(function(){
     driver.quit();
});

test.it('checking if pass field exists.',function(){
    pass.modulePass();
});
test.it('checking if name field exists.',function(){
    name.moduleName();
});
test.it('checking if email field exists.',function(){
    email.moduleMail();
});
test.it('checking if submit button exists.',function(){
    button.moduleButton();
});
});