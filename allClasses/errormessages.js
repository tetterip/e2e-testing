'use strict';
var assert = require('chai').assert;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;


class PageObjects {

	init(name,pass,email,targetclass,errormessage,y) {


		driver.findElements(By.className("pf-form__textfield__input")).then(function(elements){
		    elements.forEach(function (element, i) {
		            if(i === 0) {element.sendKeys(name)}
		           	if(i === 1) {element.sendKeys(email)}
		    });
		});

		driver.findElement(By.className("pf-form__textfield__input pf-clearer")).sendKeys(pass);
		driver.findElement(By.className('pf-form__textfield__input pf-form__textfield__input--submit')).click();
		driver.findElements(By.className(targetclass)).then(
		  	function(elements){
		  		elements.forEach(function (element, i) {
			  		var text = element.getText().then(function(text){
			  			if(i == y){expect(text).to.equal(errormessage);}
						});
		  		
		  		});
		  	});

};
};
module.exports = PageObjects;
