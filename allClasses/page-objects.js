'use strict';
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

class PageObjects {
	init(name,pass,email) {
		driver.wait(until.elementLocated(By.className("pf-form__textfield__input")),10000)
		driver.findElements(By.className("pf-form__textfield__input")).then(
			function(elements){
		    elements.forEach(function (element, i) {
		            if(i === 0) {element.sendKeys(name)}
		           	if(i === 1) {element.sendKeys(email)}
		    });
		});
		driver.findElement(By.className("pf-form__textfield__input pf-clearer")).sendKeys(pass);
		driver.findElement(By.className('pf-form__textfield__input pf-form__textfield__input--submit')).click();
		driver.sleep(3000).then(function(){driver.getCurrentUrl().then(function(url)
			{expect(url).to.equal('https://www.pollfish.com/signup/researcher')
			});
		});
	};
};
module.exports = PageObjects;