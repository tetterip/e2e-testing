'use strict';
var assert = require('assert');
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

var driver = new webdriver.Builder()
	.forBrowser('chrome')
	.build();

class PageObjects {


	init(classname) {

		driver.get('https://www.pollfish.com/dashboard/results/10578/1557829979');
	  	assert.doesNotThrow(() => { 
		  driver.wait(until.elementLocated(By.className(classname)),20000).then(
		  	function(){console.log('element exists!!');}
		  	);
		});
		driver.quit();
	}	
}
module.exports = PageObjects;