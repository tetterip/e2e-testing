//Checks if an element by a certain className exists in result page.
'use strict';
var assert = require('chai').assert;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

class PageObjects {
	init(classname) {
		driver.wait(until.elementLocated(By.className(classname)),20000);
	}	
}
module.exports = PageObjects;