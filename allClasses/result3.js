//Checks if an element exists in result page when many elements have the same class.
'use strict';
var assert = require('chai').assert;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

class PageObjects {
	init(classname1,classname2,classname3,equalto,i1) {
		driver.wait(until.elementLocated(By.className(classname2)),25000);
		driver.findElements(By.className(classname3)).then(
		  	function(elements){
		  		elements.forEach(function (element, i) {
			  		var text = element.getText().then(function(text){
			  			if(i==i1)
			  				expect(text).to.equal(equalto);;
			  		});
		  		
		  		});
		  	});
	}	
}
module.exports = PageObjects;