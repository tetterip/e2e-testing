//Clicks an element and then checks something on another one.
'use strict';
var assert = require('chai').assert;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

class PageObjects {
	init(classname,tagname1,tagname2,equalto1,equalto2,i1,i2) {
		driver.wait(until.elementLocated(By.className(classname)),20000);
		driver.findElements(By.className(tagname1)).then(
		  	function(elements){
		  		elements.forEach(function (element, i) {
			  		var text = element.getText().then(function(text){
			  			// console.log(text,i);
			  			if(text == equalto1 && i==i1)
			  				element.click();
			  		});
		  		
		  		});
		  	});
		driver.findElements(By.className(tagname2)).then(
		  	function(elements){
		  		elements.forEach(function (element, i) {
			  		var text = element.getText().then(function(text){
			  			// console.log(text,i);
			  			if(i==i2)
			  				expect(text).to.equal(equalto2);
			  		});
		  		
		  		});
		  	});
	}	
}
module.exports = PageObjects;