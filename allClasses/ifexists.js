'use strict';
var assert = require('chai').assert;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

class PageObjects {
	init(type) {
		driver.findElement(By.css(type)).then(function(element){
			var tag = element.getTagName().then(function(tag){
				expect(tag).to.equal('input');
			});
		});
	}	
}
module.exports = PageObjects;
