//Check if clicking on one rated question at the result page,changes its percentage to 100%.
var results17 = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/result1.js');

const options = {
	classname : 'react-bs-container-body',
    tagname1 : 'indicator',
    tagname2 : 'selected-item',
    equalto1 : 'A10',
    equalto2 : '100.00%',
    i1 : 38,
    i2 : 11

};

const elem = new ElementsClass(options);

elem.init(options.classname,options.tagname1,options.tagname2,options.equalto1,options.equalto2,options.i1,options.i2);
}
exports.results17 = results17;