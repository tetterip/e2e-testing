var test = require('selenium-webdriver/testing');
var selenium = require('selenium-webdriver');
var test1 = require('./resultBrowse1.js');
var test2 = require('./resultBrowse2.js');
var test3 = require('./resultBrowse3.js');
var test4 = require('./resultBrowse4.js');
var test5 = require('./resultBrowse5.js');
var test6 = require('./resultBrowse6.js');
var test7 = require('./resultBrowse7.js');
var test8 = require('./resultBrowse8.js');
var test9 = require('./resultBrowse9.js');
var test10 = require('./resultBrowse10.js');
var test11 = require('./resultBrowse11.js');
var test12 = require('./resultBrowse12.js');
var test13 = require('./resultBrowse13.js');
var test14 = require('./resultBrowse14.js');
var test15 = require('./checkAgePercentages.js');
var test16 = require('./multipleSelectionPercentage.js');
var test17 = require('./ratingStarsPercentage.js');
var test18 = require('./resultPageBrowseButton.js');
var test19 = require('./singleSelectionPercentage.js');
var test20 = require('./resultBrowse15.js');

const timeOut = '';
var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

test.describe("Result Page Testing", function(){
    this.timeout(timeOut);

test.beforeEach(function(){
	driver.get('https://www.pollfish.com/dashboard/results/10578/1557829979');
	});

test.before(function() {
  
    driver = new webdriver.Builder()
      .forBrowser('chrome')
      .build();
});

test.after(function(){
	  driver.quit();
    });


test.it("Check if in browse button,Career choice exists.", function(){
	test1.results1()});
test.it("Check if in browse button,US DMA® choice exists.", function(){
    test20.results20()});
test.it("Check if in browse button,Education choice exists.", function(){
    test2.results2()});
test.it("Check if in browse button,Employment Status choice exists.", function(){
	test3.results3()});
test.it("Check if in browse button,Income Status choice exists.", function(){
	test4.results4()});
test.it("Check if in browse button,Device Manufacturer choice exists.", function(){
    test5.results5()});
test.it("Check if in browse button,Marital Status choice exists.", function(){
    test6.results6();
});
test.it("Check if in browse button,OS choice exists.", function(){
    test7.results7();
});
test.it("Check if in browse button,Number Of Children choice exists.", function(){
    test8.results8();
});
test.it("Check if in browse button,Personas choice exists.", function(){
    test9.results9();
});
test.it("Check if in browse button,Postal Code choice exists.", function(){
    test10.results10();
});
test.it("Check if in browse button,Race choice exists.", function(){
    test11.results11();
});
test.it("Check if in browse button,US Census Division choice exists.", function(){
    test12.results12();
});
test.it("Check if in browse button,US Census Region choice exists.", function(){
    test13.results13();
});
test.it("Check if in browse button,US Congressional District choice exists.", function(){
    test14.results14();
});
test.it("Testing if checking off the box of a certain age group,turns its percent to zero.", function(){
    test15.results15();
});
test.it("Check if clicking on a multiple question at the result page,changes its percentage to 100%.", function(){
    test16.results16();
});
test.it("Check if clicking on one rated question at the result page,changes its percentage to 100%.", function(){
    test17.results17();
});
test.it("Check if browse/hide button exists.", function(){
    test18.results18();
});
test.it("Check if clicking on a single question at the result page,changes its percentage to 100%.", function(){
    test19.results19();
});
});