//Check if in browse button,Postal Code choice exists.
var results10 = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/result3.js');

const options = {
	classname1 : 'full-list-toggle btn btn-default',
	classname2 : 'rc-collapse-header',
	classname3 : 'rc-collapse-item',
	equalto : 'Postal Code',
	i1 : 10
};

const elem = new ElementsClass(options);

elem.init(options.classname1,options.classname2,options.classname3,options.equalto,options.i1);
}
exports.results10 = results10;