//Check if in browse button,US Census Region choice exists.
var results13 = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/result3.js');

const options = {
	classname1 : 'full-list-toggle btn btn-default',
	classname2 : 'rc-collapse-header',
	classname3 : 'rc-collapse-item',
	equalto : 'US Census Region',
	i1 : 13
};

const elem = new ElementsClass(options);

elem.init(options.classname1,options.classname2,options.classname3,options.equalto,options.i1);
}
exports.results13 = results13;