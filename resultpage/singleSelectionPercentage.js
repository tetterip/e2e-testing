//Check if clicking on a single question at the result page,changes its percentage to 100%.
var results19 = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/result1.js');

const options = {
	classname : 'react-bs-container-body',
    tagname1 : 'indicator',
    tagname2 : 'selected-item',
    equalto1 : 'A1',
    equalto2 : '100.00%',
    i1 : 11,
    i2 : 5

};

const elem = new ElementsClass(options);

elem.init(options.classname,options.tagname1,options.tagname2,options.equalto1,options.equalto2,options.i1,options.i2);
}
exports.results19 = results19;