//Testing if checking off the box of a certain age group,turns its percent to zero.
var results15 = function(){
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/result1.js');

const options = {
	classname : 'react-bs-container-body',
    tagname1 : 'flex-item title',
    tagname2 : 'flex-item percent',
    equalto1 : '18 - 24',
    equalto2 : '0 | 0.00%',
    i1 : 0,
    i2 : 0	
};
const elem = new ElementsClass(options);

elem.init(options.classname,options.tagname1,options.tagname2,options.equalto1,options.equalto2,options.i1,options.i2);
}
exports.results15 = results15;