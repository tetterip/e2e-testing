//Checks if user gets error message when email is missing.
var Logerror2 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/LogInError.js');

const options = {
    email:'testing@test.com',
    pass : '',
    targetclass : 'pf-form__message pf-form__message--error',
    erromessage : 'Please enter your password',
    y : 1
};
const elem = new ElementsClass(options);

elem.init(options.email,options.pass,options.targetclass,options.erromessage,options.y);
}
exports.Logerror2 = Logerror2;