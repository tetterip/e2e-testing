var test = require('selenium-webdriver/testing');
var selenium = require('selenium-webdriver');
var test1 = require('./LoginIn1.js');
var test2 = require('./LoginIn2.js');
const timeOut = 15000;

var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;	

test.beforeEach(function(){
    this.timeout(timeOut);
    driver = new webdriver.Builder()
        .forBrowser('chrome')
        .build();
	driver.get('https://www.pollfish.com/login/researcher');

	});
test.before(function() {

});

test.afterEach(function(){
	  driver.quit();
    });

test.describe("Login In Page - Error message testing", function(){

    test.it("Check if error message pops up when email field left emply", function(){
    	test1.Logerror1();
    });
    test.it("Check if error message pops up when pass field left emply", function(){
        test2.Logerror2();
    });

});