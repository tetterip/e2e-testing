//Checks if user gets error message when email is missing.
var Logerror1 = function(){
var Chance = require('chance');
var chance = new Chance();
const ElementsClass = require('/home/pollfish/e2e-ui-testing/allClasses/LogInError.js');

const options = {
    email:'',
    pass : chance.hash({length: 10}),
    targetclass : 'pf-form__message pf-form__message--error',
    erromessage : 'Please enter a valid email address.',
    y : 0
};
const elem = new ElementsClass(options);

elem.init(options.email,options.pass,options.targetclass,options.erromessage,options.y);
}
exports.Logerror1 = Logerror1;