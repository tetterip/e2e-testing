var fs = require('fs');
var request = require("request");
var assert = require('assert');
var webdriver = require('selenium-webdriver'),
   By = webdriver.By,
   until = webdriver.until;


var capabilities = {
  'browserName' : 'Firefox',
  'browser_version' : '56.0 beta',
  'os' : 'OS X',
  'os_version' : 'Sierra',
  'resolution' : '1280x1024',
  'browserstack.user' : 'tettearg1',
  'browserstack.key' : 'ii8FCfNSzpH4twFMo4nD',
  'browserstack.debug' : 'true',
  'build' : 'First build',
  'project' : 'Pollfish test 2'
};

var driver = new webdriver.Builder().
 usingServer('http://hub-cloud.browserstack.com/wd/hub').
 withCapabilities(capabilities).
 build();

var sessionId;

driver.session_.then(function(sessionData) {
   sessionId = sessionData.id_;
});

 driver.get('http://www.pollfish.com/login/researcher');
 driver.takeScreenshot().then(function(data){
    fs.writeFileSync("img1.png", data, 'base64',);
 });
 driver.sleep(2000).then(function() {
   driver.findElement(By.className('pf-form__textfield__input')).sendKeys('atetteris@iekdelta.gr')
 });
 driver.sleep(2000).then(function() {
   driver.findElement(By.className('pf-form__textfield__input pf-clearer')).sendKeys('tetterip21654');
 });
 driver.sleep(2000).then(function() {
   driver.findElement(By.className('pf-form__textfield__input pf-form__textfield__input--submit')).click();
 });
 driver.takeScreenshot().then(function(data){
    fs.writeFileSync("img2.png", data, 'base64',);
 });
 driver.findElement(webdriver.By.className('pf-btn md-button md-ink-ripple hide-xs hide-sm')).then(function(webElement) {
         console.log('Element exists');
         request({uri: "https://tettearg1:ii8FCfNSzpH4twFMo4nD@www.browserstack.com/automate/sessions/" + sessionId + ".json", method:"PUT", form:{"status":"passed","reason":"Pollfish test worked correctly"}});
     }, function(err) {
         if (err.state && err.state === 'no such element') {
             console.log('Element not found');
             request({uri: "https://tettearg1:ii8FCfNSzpH4twFMo4nD@www.browserstack.com/automate/sessions/" + sessionId + ".json", method:"PUT", form:{"status":"failed","reason":"Pollfish test didnt work correctly"}});
         } else {
             webdriver.promise.rejected(err);
         }
     });
 driver.findElement(By.className('pf-btn pf-btn--xl pf-btn--primary md-button md-ink-ripple hide-xs hide-sm')).click();
 driver.takeScreenshot().then(function(data){
    fs.writeFileSync("img3.png", data, 'base64',);
 });
 driver.findElement(webdriver.By.className('btn btn-primary btn-lg js-take-survey-btn')).then(function(webElement) {
         console.log('Element exists');
     }, function(err) {
         if (err.state && err.state === 'no such element') {
             console.log('Element not found');
         } else {
             webdriver.promise.rejected(err);
         }
     });